#import base64

#import numpy as np
import numpy as np
from flask import Flask
from flask_restful import Resource, Api, reqparse
import pandas as pd
from bs4 import BeautifulSoup
import urllib
#import ast
app = Flask(__name__)
api = Api(app)
import datetime
import json
import yaml
from dateutil.relativedelta import relativedelta
from collections import defaultdict
import requests
import re
#import asyncio
#from pyppeteer import launch
import feedparser
import dataframe_image as dfi
from flask import send_file
import uuid
import os
import time
from io import BytesIO
import cv2

class Rtv(Resource):
    def get(self):

        long_words_short = {'posnetek': "pos.",
                            'koncerta':"konc.",
                            'intervju':'int.',
                            'orkester':"ork.",
                            "glasbeni":"glas.",
                            "družinski":"druž.",
                            "slovenija":"Slo.",
                            "življenje":"živ.",
                            "ljubljana":"Ljub.",
                            "digitalna":"dig.",
                            "slovenska":"slov.",
                            "politično":"pol.",
                            "slovenski":"slov.",
                            "slovencev":"slov.",
                            "tehnološka":"teh.",
                            "bolnišnica":"bol.",
                            "simfonični":"simf.",
                            "strategija":"strat.",
                            "maribor":"Mar.",
                            "mariborske":"Mar.",
                            "arhitektura":"arh.",
                            "znanstveni":"znan.",
                            "znanost":"znan.",
                            "tehnologije":"teh.",
                            "filharmonije":"fil.",
                            "dokumentarni":"dok.",
                            "razvedrilna":"razv.",
                            "infrastrukturo":"infr.",
                            "izobraževanje":"izob.",
                            "kvalifikacije":"kval.",
                            "slovensko":"slov.",
                            "slovenske":"slov.",
                            }

        parser = reqparse.RequestParser()  # initialize

        #https://api.rtvslo.si/spored/getProvys/TVS1/2021-05-090
        parser.add_argument('channel', required=True)  # add args
        #parser.add_argument('date', required=True)

        args = parser.parse_args()  # parse arguments to dictionary


        today = datetime.datetime.today()
        url = 'https://api.rtvslo.si/spored/getProvys/'+args["channel"]+"/"+datetime.date.strftime(today,"%Y-%m-%d")
        print(url)
        response = urllib.request.urlopen(url)
        data = response.read()  # a `bytes` object
        text = data.decode('utf-8')  # a `str`;

        soup = BeautifulSoup(text, 'xml')
        programmes = soup.find_all('PROGRAMME')

        dict = defaultdict(list)
        for program in soup.find_all('PROGRAMME'):
            test = program.find("SLO")
            if test is None:
                continue
            else:
                prog = str(program.find("SLO").string)
                prog_all = program.find_all("SLO")
                if len(prog_all) >1:
                    if len(prog_all[1].string)>20:
                        prog= prog+":"+prog_all[1].string[:50]
                    else:
                        prog= prog+":"+prog_all[1].string
                #print(prog)
                for long_word in long_words_short:
                    if long_word.lower() in prog.lower():
                        #print(long_word)
                        #print(prog)
                        prog = prog.replace(long_word, long_words_short[long_word])
                        prog = prog.replace(long_word.capitalize(), long_words_short[long_word])
                        #print(prog)
                dict[program["SORT_NUMBER"]].append(str(program["BILLEDSTART"]))  # .strip("<SLO>").strip("</SLO>")
                dict[program["SORT_NUMBER"]].append(prog[0].capitalize()+prog[1:])  # .strip("<SLO>").strip("</SLO>")
        # print (name.get_text())
        #print(dict)

        sorted_d = sorted(dict.items(), key=lambda item: int(item[0]))

        json_response = json.dumps(sorted_d, ensure_ascii=False)
        #print(json_response)
        ud = yaml.safe_load(json_response)
        #print(ud)
        return ud

class Ntk(Resource):
    def get(self):

        regex = "^\d{2}\.\d{2}\.\d{4}.*"

        url='https://ntzs.si/ntzs/?bsjlapageid=natunion&union=ntzs&action=calendar=1%7Cexcel=calendar'
        df=pd.read_excel(url, sheet_name='Worksheet', engine='openpyxl')

        df.rename(columns={"Unnamed: 1":"datum","Unnamed: 2":"kraj","Unnamed: 3":"kategorija","Unnamed: 4":"tekmovanje"}, inplace=True)

        df = df.loc[df["datum"].str.contains(regex,na=False), ["datum","kraj","kategorija","tekmovanje"]]
        #df.dropna(subset=["kraj"],inplace=True)


        date = datetime.datetime.now()
        date_plus_1 = date + relativedelta(years=1)
        date_minus_1_day = date - relativedelta(days=1)

        df["datum"] = df["datum"].str.replace("\n",",")
        df["datum"] = df["datum"].str.replace("."+date.strftime('%Y'),"."+date.strftime('%y'))
        df["datum"] = df["datum"].str.replace("."+date_plus_1.strftime('%Y'),"."+date_plus_1.strftime('%y'))

        df["kraj"] = df["kraj"].str.replace("(SLO - Slovenija)","SLO")
        df["kraj"] = df["kraj"].str.replace("\n",",")
        df["kraj"] = df["kraj"].str.replace('nan',"-")

        df["tekmovanje"] = df["tekmovanje"].str.replace("za moške","M")
        df["tekmovanje"] = df["tekmovanje"].str.replace("za ženske","Ž")
        df["tekmovanje"] = df["tekmovanje"].str.replace(" krog","")
        df["tekmovanje"] = df["tekmovanje"].str.replace("Državno prvenstvo","DP")
        df["tekmovanje"] = df["tekmovanje"].str.replace("prvenstvo","prv.")
        df["tekmovanje"] = df["tekmovanje"].str.replace("posameznike","pos.")
        df["tekmovanje"] = df["tekmovanje"].str.replace("članice in člane U-21","čl. U-21")
        df["tekmovanje"] = df["tekmovanje"].str.replace("Kvalifikacije","Kval.")
        df["tekmovanje"] = df["tekmovanje"].str.replace("mlajše kadetinje in kadete","MK")
        df["tekmovanje"] = df["tekmovanje"].str.replace("Evropsko","Evr.")
        df["tekmovanje"] = df["tekmovanje"].str.replace("Finale","Fin.")
        df["tekmovanje"] = df["tekmovanje"].str.replace("dvojice","dvoj.")
        df["tekmovanje"] = df["tekmovanje"].str.replace("veteransko","vet.")

        slovar = defaultdict(str)

        datum = ""
        vejica =","

        df.reset_index(inplace=True)

        counter = 0
        for index, row in df.iterrows():
            if "SNTL" in row["tekmovanje"]:
                if counter <= 30 and (datetime.datetime.strptime(row["datum"][:8],"%d.%m.%y") >= date_minus_1_day):
                    slovar[row["datum"]]+=row["tekmovanje"]+vejica
                    counter+=1
            elif row["datum"]:
                if counter <= 30 and (datetime.datetime.strptime(row["datum"][:8],"%d.%m.%y") >= date_minus_1_day):
                    slovar[row["datum"]]+=row["tekmovanje"]+","+str(row["kraj"]).replace("nan","-")+vejica
                    counter+=1

        for key,value in slovar.items():
            slovar[key]=value.rstrip(",")

        list_of_matches =[]
        for k,v in slovar.items():
            list_of_matches.append(k+" "+v)

        final =[]
        for e in list_of_matches:
            f =e.replace("."+date.strftime('%y'),"")
            final.append(f.replace("."+date_plus_1.strftime('%y'),""))

        json_response = json.dumps(final, ensure_ascii=False)
        #print(json_response)
        ud = yaml.safe_load(json_response)
        #print(ud)
        return ud
class Bus(Resource):
    def get(self):

        parser = reqparse.RequestParser()  # initialize

        #https://api.rtvslo.si/spored/getProvys/TVS1/2021-05-090
        parser.add_argument('from', required=True)  # add args
        parser.add_argument('to', required=True)  # add args
        #parser.add_argument('date', required=True)

        args = parser.parse_args()  # parse arguments to dictionary

        session = requests.Session()
        session.get("http://www.vozovnica.si/vozni_red/ajax/vstop.php?VSTOP_IME="+args['from'])
        session.get("http://www.vozovnica.si/vozni_red/ajax/izstop.php?IZSTOP_IME="+args['to'])

        page_html = session.get("http://www.vozovnica.si").text
        df_list = pd.read_html(page_html)
        df = df_list[1]

        df['Prevoznik'] = df['Prevoznik'].str.replace("Javno podjetje Ljubljanski potniški promet, d.o.o.","LPP")
        df['Prevoznik'] = df['Prevoznik'].str.replace("NOMAGO, storitve mobilnosti in potovanj, d.o.o.","NOM")
        df['Prevoznik'] = df['Prevoznik'].str.replace("ARRIVA DOLENJSKA IN PRIMORSKA d.o.o. Poslovna enota Ljubljana","ARR")
        df['Prevoznik'] = df['Prevoznik'].str.replace("ARRIVA DOLENJSKA IN PRIMORSKA d.o.o.","ARR")
        json_response = (df['Časodhoda']+" "+df['Prevoznik']).to_list()

        return json_response

class Vlak(Resource):
    def get(self):

        parser = reqparse.RequestParser()  # initialize

        #https://api.rtvslo.si/spored/getProvys/TVS1/2021-05-090
        parser.add_argument('from', required=True)  # add args
        parser.add_argument('to', required=True)  # add args
        #parser.add_argument('date', required=True)

        args = parser.parse_args()  # parse arguments to dictionary

        url ="https://potniski.sz.si/vozni-red"
        session = requests.Session()

        page_html = session.get(url).text

        soup = BeautifulSoup(page_html, "html.parser")


        d = {}

        for s in soup.find_all('option'):
            #print(s.string)
            #print(s['value'])
            d[s.string] = s['value']

        page_html = session.get("https://potniski.sz.si/vozni-red/?action=timetables_search&current-language=sl&entry-station="+d[args['from']]+"&exit-station="+d[args['to']]).text

        df_list = pd.read_html(page_html)
        #print(df_list[0])

        schedule = []
        for index, row in df_list[0].iterrows():
            pos = re.search(r"\d",row['Postaja']).start()
            schedule.append(row['Postaja'][pos:pos + 5] + " " + row['Vrsta vlaka'])

        return schedule

class Prijavim(Resource):
    def get(self):

        parser = reqparse.RequestParser()  # initialize


        parser.add_argument('filter_discipline', required=True)  # add args

        args = parser.parse_args()  # parse arguments to dictionary

        url ="https://prijavim.se/calendar/search_results/?filter_discipline=0&filter_legaue=-1"
        session = requests.Session()

        page_html = session.get(url).text

        soup = BeautifulSoup(page_html, "html.parser")


        dict = {}

        for select in soup.find_all('select' , {'name':'filter_discipline'}):
            for d in select.find_all('option'):
         #       print(d.string)
          #      print(d['value'])
                dict[d.string] = d['value']

        #print(dict)

        ht =requests.get("https://prijavim.se/calendar/search_results/?filter_discipline="+dict[args['filter_discipline']]+"&filter_legaue=-1", timeout=None)
        #print(ht.text)
        page_html = session.get("https://prijavim.se/calendar/search_results/?filter_discipline="+dict[args['filter_discipline']]+"&filter_legaue=-1", timeout=(3.05,5)).text
 #       print(page_html)
        soup = BeautifulSoup(page_html, "html.parser")



        for f in soup.find_all('span' , {'class':'hide-me'}):

            #print(event.string)
            #print(event['value'])
            #dict[d.string] = d['value']

            #   if title != "None":
            #print(start.text)
            c = f.text
#            print(c)




        i =0
        events = []
        filter =".*disicpline-"+dict[args["filter_discipline"]]+".*" #typo indended
        print(filter)
        if args["filter_discipline"] ==  "Vse discipline":
            filter =".*"

        for filter_d in soup.find_all('div' , {'class':re.compile(filter)}):
            for event_t in filter_d.find_all('span' , {'class':'evcal_desc2 evcal_event_title'}):

            #print(event.string)
            #print(event['value'])
            #dict[d.string] = d['value']

            #   if title != "None":
            #print(start.text)
                event = event_t.text.strip()
                #print(event)
                events.append(event)

        """for start in soup.find_all('span' , {'class':'evcal_desc2 evcal_event_title'}):

            #print(event.string)
            #print(event['value'])
            #dict[d.string] = d['value']

            #   if title != "None":
            #print(start.text)
            event = start.text.strip()
            events.append(event)"""

        months = { " Jan "  : ".01"," Feb " : ".02"," Mar " : ".03"," Apr " : ".04"," May " : ".05"," Jun " : ".06"," Jul " : ".07"," Aug " : ".08"," Sep " : ".09"," Oct " : ".10"," Nov " : ".11"," Dec " : ".12"}

        date_location = []

        for filter_t in soup.find_all('div' , {'class':re.compile(filter)}):
            for start in filter_t.find_all('span' , {'class':'start'}):

                #print(event.string)
                #print(event['value'])
                #dict[d.string] = d['value']

                    #   if title != "None":
                #print(start.text)
                t = start.text
                day = start.text.split(" ")[1][0:2]

                date = datetime.datetime.now()
                date_plus_1 = date + relativedelta(years=1)




                t = t.replace("PO","").replace("TO","").replace("SR","").replace("ČE","").replace("PE","").replace("SO","").replace("NE","").replace("\n","").replace("\t","").replace(u'\xa0'," ").replace("  "," ").replace(date.strftime('%Y'),"").replace(date_plus_1.strftime('%Y'),"")

                for k,v in months.items():
                    if k in t:
                        t=t.replace(k,months[k])
                #print(t)
                date_location.append(t.split(" ")[0]+","+day+" "+" ".join(t.split(" ")[1:]))

        json_response = []

        for index,e in enumerate(events):
            if index > 30:
                break
            t=0
            json_response.append(date_location[index].split(" ")[0]+" "+events[index]+", "+" ".join(date_location[index].split(" ")[1:]))
        return json_response

"""async def main_get(id):
    browser = await launch(options={"args": ['--no-sandbox']},handleSIGINT=False,handleSIGTERM=False,handleSIGHUP=False)
    [page] = await browser.pages()
    #await page.goto("https://practiscore.com/results/new/137895")
    #print("https://practiscore.com/results/new/137895")
    #print("https://practiscore.com/results/new/"+str(id))
    await page.goto("https://practiscore.com/results/new/"+str(id))
    #await page.goto("http://www.vozovnica.si/vozni_red/ajax/izstop.php?IZSTOP_IME=LJUBLJANA%20AVTOBUSNA%20POSTAJA")
    #await page.goto("http://www.vozovnica.si")
    #print(await page.content()) # shows that the `<p>` was inserted
    df_list = pd.read_html(await page.content())
    #df_list = pd.read_html(await page.content())
    #print(df_list)
    df = df_list[1]
    # evaluate a JS expression in browser context and scrape the data
    #expr = "document.querySelector('p').textContent"
    #print(await page.evaluate(expr, force_expr=True)) # => hello world
    await browser.close()
    if df_list:
        return df_list[1]
    else:
        return []
"""

"""class PractiScore(Resource):


    def get(self):

        parser = reqparse.RequestParser()  # initialize


        parser.add_argument('id', required=True)  # add args
        parser.add_argument('name', required=True)  # add args

        args = parser.parse_args()  # parse arguments to dictionary



        json_response = []

        for i in range(5):
            asyncio.set_event_loop(asyncio.new_event_loop())


            loop = asyncio.get_event_loop()
            df = loop.run_until_complete(main_get(str(args["id"])))
            #print(df)

            if 'Name' in df.columns:

                #print(df)
                #print(df.columns)
                for  index,row in df.iterrows():
                    if index > 100:
                        break
                    print(row[df.columns[0]]+" "+str(row[df.columns[1]])+str(df.columns[1]))
                    if args['name'] in row[df.columns[0]]:
                        json_response.insert(0,"RANKINGS:")
                        result_person =""
                        result_person = ", ".join(str(e)+":"+str(row[e]) for e in df.columns if e != "Name")
                        result_person = row[df.columns[list(df.columns).index("Name")]]+","+result_person
                        json_response.insert(0,result_person)

                    if "Name" in df.columns and "%" in df.columns and "Div" in df.columns and "Class" in df.columns:
                        print(df.columns)

                        json_response.append(row[df.columns[list(df.columns).index("Name")]]+", "+str(df.columns[list(df.columns).index("%")]+":"+str(row[df.columns[list(df.columns).index("%")]]))
                                             +", "+str(df.columns[list(df.columns).index("Div")])+":"+str(row[df.columns[list(df.columns).index("Div")]])+
                                             ", "+str(df.columns[list(df.columns).index("Class")])+":"+str(row[df.columns[list(df.columns).index("Class")]]))
                    else:
                        json_response.append(row[df.columns[list(df.columns).index("Name")]]+", "+str(df.columns[list(df.columns).index("%")]+":"+str(row[df.columns[list(df.columns).index("%")]])))

                break


        return json_response
"""
class Tk(Resource):


    def get(self):



        url ="https://tk-logatec.sportifiq.com/en"
        session = requests.Session()

        page_html = session.get(url).text

        soup = BeautifulSoup(page_html, "html.parser")


        d = defaultdict(list)

        """for s in soup.find_all('div', {'id':"sport_venue_885"}):
            for sp in s.find_all('span'):
                print(sp.string)"""


        for s in soup.find_all('div', {'title':"Booked"}):
            for sp in s.find_all('span'):
                print(s.attrs['id'])
                if "_885_" in s.attrs['id']:
                    d["A"].append(s.attrs['id'].split("_")[1].split("-")[2]+"."+s.attrs['id'].split("_")[1].split("-")[1]+" "+s.attrs['id'].split("_")[-1].strip().replace("-0","")+ "h, "+sp.string.strip().replace("TK Logat...","TK Logatec"))
                else:
                    d["B"].append(s.attrs['id'].split("_")[1].split("-")[2]+"."+s.attrs['id'].split("_")[1].split("-")[1]+" "+s.attrs['id'].split("_")[-1].strip().replace("-0","")+ "h, "+sp.string.strip().replace("TK Logat...","TK Logatec"))
                print(sp.string)

        print(d)


        return [d["A"],d["B"]]

class RSS(Resource):

    def get(self):

        parser = reqparse.RequestParser()  # initialize


        parser.add_argument('id', required=True)  # add args

        args = parser.parse_args()  # parse arguments to dictionary

        response = []
        news_feed = feedparser.parse("https://img.rtvslo.si/feeds/"+args['id']+".xml")

        for k in news_feed['entries']:

            response.append(k['title'])

        return response

class Sava(Resource):

    def get(self):

        session = requests.Session()

        json_response =[]

        page_html = session.get("https://www.marketwatch.com/investing/stock/posr?countrycode=si").text
        soup = BeautifulSoup(page_html, "html.parser")

        price = soup.find('meta',{'name': 'price'})#('bg-quote',{'class': 'value'})
        price_today=price['content']

        print(price_today)
        json_response.append(price_today.strip("€")+"€")


        change_point =  soup.find('span',{'class': 'change--point--q'})
        print(change_point.string)
        json_response.append(change_point.string+"€")

        change_percent =  soup.find('span',{'class': 'change--percent--q'})
        print(change_percent.string)
        json_response.append(change_percent.string)


        df_list = pd.read_html(page_html)
        df = df_list[1]

        df_list[4]["Con"]=df_list[4][0]+" "+df_list[4][1]
        json_response+=df_list[4]['Con'].to_list()

        #price_today = df_list[1]['Previous Close'].to_list()[0].strip("€")+"€"
        #json_response.insert(0,price_today)




        return json_response

class Excel(Resource):
    """MAX_COLS = 30
    MAX_ROWS = 100

    def export(obj, filename, fontsize=14, max_rows=None, max_cols=None,
               table_conversion='chrome', chrome_path=None):
        return _export(obj, filename, fontsize, max_rows, max_cols, table_conversion, chrome_path)


    def _export(obj, filename, fontsize, max_rows, max_cols, table_conversion, chrome_path):
        is_styler = isinstance(obj, Styler)
        df = obj.data if is_styler else obj


        html = obj.to_html(max_rows=max_rows, max_cols=max_cols, notebook=True)

        from matplotlib_table import TableMaker
        converter = TableMaker(fontsize=fontsize, encode_base64=False, for_document=False).run
        img_str = converter(html)

        if isinstance(filename, str):
            open(filename, 'wb').write(img_str)
        elif hasattr(filename, 'write'):
            filename.write(img_str)"""

    def get(self):



        cwd =os.getcwd()
        #print(cwd)
        os.chdir(cwd)
        path = "OUT/"
        if not os.path.exists(path):
            os.makedirs(path)

        #delete older files
        now = time.time()
        for filename in os.listdir(path):
            filestamp = os.stat(os.path.join(path, filename)).st_mtime
            filecompare = now - 0.1 * 86400
            #print(filename)
            if  filestamp < filecompare and (filename.endswith(".jpg") or filename.endswith(".png")):
                os.remove(path+filename)

        parser = reqparse.RequestParser()  # initialize


        parser.add_argument('url', required=True)  # add args
        parser.add_argument('rows', required=True)  # add args
        parser.add_argument('columns', required=True)  # add args
        parser.add_argument('sheet', required=True)  # add args
        parser.add_argument('header', required=True)  # add args
        parser.add_argument('engine', required=True)  # add args
        parser.add_argument('gradient', required=True)  # add args
        parser.add_argument('invert', required=False)  # add args
        parser.add_argument('sort', required=False)  # add args


        args = parser.parse_args()  # parse arguments to dictionary

        #url='https://ntzs.si/ntzs/?bsjlapageid=natunion&union=ntzs&action=calendar=1%7Cexcel=calendar'
        if args['engine'] == 'csv':
            if "docs.google.com" in args['url']:
                args['url'] = args['url'].replace("/edit#gid=", "/export?format=csv&gid=")
            df=pd.read_csv(args['url'],  header=int(args["header"]), nrows=int(args['rows']),usecols=[x for x in range(0, int(args['columns']))])
        else:
            df=pd.read_excel(args['url'], engine=args['engine'], header=int(args["header"]), sheet_name=args['sheet'], nrows=int(args['rows']),usecols=[x for x in range(0, int(args['columns']))])

        if args['sort'] is not None:
            if args['sort'] != "0":
                if args['sort'][0] == "A":
                    df = df.sort_values(by=df.columns[int(args['sort'][1:])])
                elif args['sort'][0] == "D":
                    df = df.sort_values(by=df.columns[int(args['sort'][1:])], ascending=False)


        if int(args['gradient']):
            df = df.style.background_gradient()
        uuid_string = uuid.uuid4().hex
        image_path = path+uuid_string+".png"
        dfi.export(df,image_path, table_conversion="other")

        image = cv2.imread(image_path)
        if args['invert'] is not None:
            if int(args['invert']):
                image = np.invert(image)



        cv2.imwrite(path+uuid_string+".jpg",image, [int(cv2.IMWRITE_JPEG_QUALITY),100])

        return send_file(path+uuid_string+".jpg",mimetype= 'image/jpeg')

api.add_resource(Rtv, '/rtv')  # add endpoints
api.add_resource(Ntk, '/ntk')  # add endpoints
api.add_resource(Bus, '/bus')  # add endpoints
api.add_resource(Vlak,'/vlak')  # add endpoints
api.add_resource(Prijavim,'/prijavim')  # add endpoints
#api.add_resource(PractiScore,'/practi')  # add endpoints
api.add_resource(Tk,'/tk')  # add endpoints
api.add_resource(RSS,'/rss')  # a endpoints
api.add_resource(Sava,'/sava')  # add endpoints
api.add_resource(Excel,'/excel')  # add endpoints
if __name__ == '__main__':
    app.run(debug=False)  # run our Flask app